﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class Waypoint : MonoBehaviour
{
  private List<Transform> _neighbours;

  public void Refresh(bool refreshNeighbours)
  {
    // Anything overlapping within a 0.45f radius, excluding this
    var neighbours = Physics2D
      .OverlapCircleAll(transform.position, 0.45f, Layers.Buildings)
      .Where(c => c.transform != this.transform);

    _neighbours = new List<Transform>();

    foreach (Transform neighbour in neighbours.Select(c => c.transform))
    {
      _neighbours.Add(neighbour);

      var roadNeighbour = neighbour.GetComponent<Waypoint>();
      if (roadNeighbour != null && refreshNeighbours)
        roadNeighbour.Refresh(false);
    }
  }

  void OnDrawGizmos()
  {
    foreach (Transform neighbour in _neighbours)
      Gizmos.DrawLine(transform.position, neighbour.position);
  }
}
