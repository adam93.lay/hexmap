﻿using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class Building : MonoBehaviour
{
  public string Name;
  public Sprite Sprite;
  public float Cost;
  public int MapRange;
  public bool BuildMultiple;
  public bool NotifyOnComplete;

  private string _customName;
  public string CustomName => _customName ?? Name;

  void Start()
  {
    GetComponent<SpriteRenderer>().sprite = Sprite;
  }
}
