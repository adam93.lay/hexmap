﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Inventory))]
public class Factory : MonoBehaviour
{
  private Inventory _inv;

  void Start()
  {
    _inv = GetComponent<Inventory>();
  }

  private float _timer = 0f;
  private float _testTimerTime = 10f;

  void Update()
  {
    _timer += Time.deltaTime;

    // If the timer has elapsed
    if (_timer >= _testTimerTime)
    {
      // Reset the timer
      _timer = 0;

      OnInterval();
    }
  }

  void OnInterval()
  {
    // Just have the factory generate some money
    MoneyManager.Instance.Balance += 5_000f;

    _inv.AddItem(new Item());
  }
}
