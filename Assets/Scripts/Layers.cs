﻿using UnityEngine;

public static class Layers
{
  public static int Map => FromName(nameof(Map));
  public static int Buildings => FromName(nameof(Buildings));

  private static int FromName(string name) => 1 << LayerMask.NameToLayer(name);
}
