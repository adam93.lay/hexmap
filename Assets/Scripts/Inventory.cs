﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class Inventory : MonoBehaviour
{
  private readonly List<Item> _items = new List<Item>();

  public void AddItem(Item item)
  {
    _items.Add(item);
  }

  public void RemoveItem(Item item)
  {
    _items.Remove(item);
  }
}
