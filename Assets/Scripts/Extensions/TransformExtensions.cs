﻿using UnityEngine;

public static class TransformExtensions
{
  public static Transform TryGetChild(this Transform t, int index)
  {
    try
    {
      return t.GetChild(index);
    }
    catch
    {
      return null;
    }
  }
}
