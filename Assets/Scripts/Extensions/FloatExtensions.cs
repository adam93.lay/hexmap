﻿public static class FloatExtensions
{
  public static string ToMoney(this float money)
  {
    const string currencySymbol = "£";

    if (money >= 1_000_000)
      return currencySymbol + (money / 1_000_000) + "M";

    if (money >= 1_000)
      return currencySymbol + (money / 1_000) + "K";

    return currencySymbol + (money).ToString();
  }
}
