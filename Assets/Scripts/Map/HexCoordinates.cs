﻿using UnityEngine;

[System.Serializable]
public struct HexCoordinates
{
  public int X { get; }
  public int Y { get; }

  public HexCoordinates(int x, int y)
  {
    X = x;
    Y = y;
  }

  public HexCoordinates(Node node)
    : this(node.X, node.Y) { }

  public static HexCoordinates FromGridCoordinates(int x, int y)
  {
    return new HexCoordinates(x, y);
  }

  public Vector2 ToVector2()
  {
    return new Vector2((X + Y * 0.5f - Y / 2) * (HexHelper.InnerRadius * 2f),
      Y * (HexHelper.OuterRadius * 1.5f));
  }

  public override string ToString()
  {
    return $"({X}, {Y})";
  }
}