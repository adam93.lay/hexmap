﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Map : MonoBehaviour
{
  public int Width = 15;
  public int Height = 10;

  public MapCell CellPrefab;
  public Text LabelPrefab;

  private Canvas _labelCanvas;
  private MapCell[,] _mapCells;
  private static Node[,] _graph;
  public static Node[,] Graph => _graph;
  private Dictionary<string, MapCell> _mapCellPrefabs;
  private Dictionary<string, MapCellType> _mapCellTypes;

  void Awake()
  {
    _mapCellPrefabs = Resources.LoadAll<MapCell>("Prefabs/Map")
      .ToDictionary(p => p.name, p => p);

    _mapCellTypes = Resources.LoadAll<MapCellType>("Templates/Map")
      .ToDictionary(p => p.Name, p => p);

    _labelCanvas = GetComponentInChildren<Canvas>();

    _mapCells = new MapCell[Width, Height];

    for (int x = 0; x < Width; x++)
      for (int y = 0; y < Height; y++)
        InitCell(x, y);

    GenerateGraph();
  }

  private void InitCell(int x, int y)
  {
    MapCellType type = _mapCellTypes["Dirt"];

    #region Test map generating...

    if (x == 2)
      if (new[] { 1, 2, 3 }.Contains(y))
        type = _mapCellTypes["Mars"];
    if (x == 3 && y == 3)
      type = _mapCellTypes["Mars"];

    if (x == 5 || x == 7)
      if (new[] { 5, 6, 7, 8, 9 }.Contains(y))
        type = _mapCellTypes["Grass"];
    if (x == 6 && y == 6)
      type = _mapCellTypes["Grass"];

    #endregion

    MapCell cell = _mapCells[x, y] = CreateMapCell(x, y, type);

    if (_labelCanvas != null)
    {
      Text label = Instantiate(LabelPrefab);
      label.rectTransform.SetParent(_labelCanvas.transform, false);
      label.rectTransform.anchoredPosition = cell.Coordinates.ToVector2();
      label.text = cell.Coordinates.ToString();
    }
  }

  private MapCell CreateMapCell(int x, int y, MapCellType type)
  {
    var coords = new HexCoordinates(x, y);
    Vector2 pos = coords.ToVector2();

    MapCell cellGo = Instantiate(_mapCellPrefabs["MapCell"], transform, false);
    cellGo.MapCellType = type;
    cellGo.transform.localPosition = pos;
    cellGo.Coordinates = coords;
    return cellGo;
  }

  private void GenerateGraph()
  {
    _graph = new Node[Width, Height];

    for (int x = 0; x < Width; x++)
      for (int y = 0; y < Height; y++)
      {
        var cellType = _mapCells[x, y].MapCellType;
        _graph[x, y] = new Node(x, y, cellType.CanMoveOver ? cellType.MovementCost : float.PositiveInfinity);
      }


    // TODO: Refactor this
    for (int x = 0; x < Width; x++)
      for (int y = 0; y < Height; y++)
      {
        var n = _graph[x, y];

        // If not on left side of map, add left neighbour
        if (x > 0)
          n.Edges.Add(_graph[x - 1, y]);
        // If not on right side of map, add right neighbour
        if (x < Width - 1)
          n.Edges.Add(_graph[x + 1, y]);

        bool isOddRow = y % 2 == 1;
        // If it's an even row, the top/bottom neighbours are x -1 and x
        // If it's an odd row, the top/bottom neighbours are x and x + 1
        int leftVertical = isOddRow ? x : x - 1;
        int rightVertical = isOddRow ? x + 1 : x;

        // If not on the bottom of the map, add bottom neighbours
        if (y > 0)
        {
          // If left bottom is not off the left side
          if (leftVertical > 0)
            n.Edges.Add(_graph[leftVertical, y - 1]);
          // If right bottom is not off the right side
          if (rightVertical < Width - 1)
            n.Edges.Add(_graph[rightVertical, y - 1]);
        }

        if (y < Height - 1)
        {
          // If left top is not off the left side
          if (leftVertical > 0)
            n.Edges.Add(_graph[leftVertical, y + 1]);
          // If right top is not off the right side
          if (rightVertical < Width - 1)
            n.Edges.Add(_graph[rightVertical, y + 1]);
        }
      }
  }
}
