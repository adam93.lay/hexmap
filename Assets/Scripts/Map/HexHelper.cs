﻿using UnityEngine;

public static class HexHelper
{
  public const float OuterRadius = 0.5f;
  public const float InnerRadius = 0.4330127019f;

  public static Vector2[] Corners = {
    new Vector2(0f, OuterRadius),
    new Vector2(InnerRadius, 0.5f * OuterRadius),
    new Vector2(InnerRadius, -0.5f * OuterRadius),
    new Vector2(0f, -OuterRadius),
    new Vector2(-InnerRadius, -0.5f * OuterRadius),
    new Vector2(-InnerRadius, 0.5f * OuterRadius)
  };
}
