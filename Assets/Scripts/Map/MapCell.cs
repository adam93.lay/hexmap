﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class MapCell : MonoBehaviour
{
  public MapCellType MapCellType;
  public HexCoordinates Coordinates;

  void Start()
  {
    GetComponent<SpriteRenderer>().sprite = MapCellType.Sprite;
  }

  public Building GetBuilding()
  {
    Transform child = transform.TryGetChild(0);

    return child?.GetComponent<Building>();
  }

  void OnMouseUp()
  {
    SelectionManager.OnSelection?.Invoke(gameObject);
  }

  void OnDrawGizmos()
  {
    //Gizmos.DrawSphere(transform.position, 0.15f);
  }
}
