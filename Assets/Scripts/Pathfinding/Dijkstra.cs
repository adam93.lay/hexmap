﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class Dijkstra
{
  public (bool success, List<Node> path) Calculate(Node[,] graph, Node source, Node target)
  {
    Dictionary<Node, float> dist = new Dictionary<Node, float>();
    Dictionary<Node, Node> prev = new Dictionary<Node, Node>();

    //Node[] unvisited = new Node[graph.GetLength(0) * graph.GetLength(1)];
    List<Node> unvisited = new List<Node>();

    dist[source] = 0f;
    prev[source] = null;

    foreach (Node v in graph)
    {
      if (v != source)
      {
        dist[v] = float.PositiveInfinity;
        prev[v] = null;
      }

      unvisited.Add(v);
    }

    while (unvisited.Any())
    {
      Node u = null;
      foreach (Node possible in unvisited)
        if (u == null || dist[possible] < dist[u])
          u = possible;

      if (u == target)
        break;

      unvisited.Remove(u);

      foreach (Node v in u.Edges)
      {
        float alt = dist[u] + u.DistanceTo(v);
        if (v == null || !dist.ContainsKey(v))
        {

        }
        if (alt < dist[v])
        {
          dist[v] = alt;
          prev[v] = u;
        }
      }
    }

    if (prev[target] == null)
      return (false, null);

    var path = new List<Node>();
    Node current = target;
    while (current != null)
    {
      path.Add(current);
      current = prev[current];
    }

    path.Reverse();

    return (true, path);
  }
}
