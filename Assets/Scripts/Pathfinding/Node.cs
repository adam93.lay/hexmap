﻿using System.Collections.Generic;

public class Node
{
  public List<Node> Edges { get; }
  public int X { get; }
  public int Y { get; }
  public float Cost { get; }

  public Node(int x, int y, float cost)
  {
    X = x;
    Y = y;
    Cost = cost;
    Edges = new List<Node>();
  }

  public float DistanceTo(Node node)
  {
    return 1 * Cost; // Does this matter? All tiles are equidistant
  }
}
