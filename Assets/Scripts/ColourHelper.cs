﻿using UnityEngine;

public static class ColourHelper
{
  public static Color FromRgba(float r, float g, float b, float a = 255f)
  {
    return new Color(r / 255f, g / 255f, b / 255f, a / 255f);
  }
}
