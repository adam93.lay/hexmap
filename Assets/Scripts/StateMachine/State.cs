﻿using UnityEngine;

namespace StateMachine
{
  [CreateAssetMenu(menuName = "AI/State")]
  public abstract class State : ScriptableObject
  {
    public Action[] Actions;

    public void Update(StateController stateController)
    {
      DoActions(stateController);
    }

    private void DoActions(StateController stateController)
    {
      foreach (var action in Actions)
      {
        action.Act(stateController);
      }
    }
  }
}
