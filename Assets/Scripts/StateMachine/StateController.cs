﻿using UnityEngine;

namespace StateMachine
{
  public class StateController : MonoBehaviour
  {
    public State CurrentState;

    void Update()
    {
      CurrentState.Update(this);
    }
  }
}
