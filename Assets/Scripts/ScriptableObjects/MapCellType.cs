﻿using System;
using UnityEngine;

[CreateAssetMenu]
public class MapCellType : ScriptableObject
{
  public string Name;
  public Sprite Sprite;
  public float MovementCost;
  public bool CanMoveOver;
}
