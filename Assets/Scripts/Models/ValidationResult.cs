﻿public struct ValidationResult
{
  public bool Valid { get; }
  public string Error { get; }

  public ValidationResult(bool valid, string error = null)
  {
    Valid = valid;
    Error = error;
  }
}
