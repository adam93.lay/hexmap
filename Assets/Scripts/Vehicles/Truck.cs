﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class Truck : MonoBehaviour
{
  public float Speed = 2f;

  private Queue<Node> _lastPath;

  void Start()
  {

  }

  private bool _moving;

  void Update()
  {
    // For test!
    if (Input.GetKeyDown(KeyCode.W))
    {
      var mapHit = InputManager.GetMouseHit(Layers.Map);
      if (mapHit.collider != null)
      {
        MoveToCell(mapHit.transform.GetComponent<MapCell>());
      }
    }

    if (_lastPath != null && _lastPath.Any())
    {
      if (!_moving)
      {
        StartCoroutine(MoveToNextNode(_lastPath.Dequeue()));
      }
    }
  }

  IEnumerator MoveToNextNode(Node node)
  {
    _moving = true;

    Vector2 start = transform.position;
    Vector2 dest = new HexCoordinates(node).ToVector2();
    float lerpTime = 0;
    while (lerpTime <= 1)
    {
      lerpTime += Time.deltaTime * Speed;
      transform.position = Vector2.Lerp(start, dest, lerpTime);
      yield return 0;
    }

    _moving = false;
  }

  public void MoveToCell(MapCell cell)
  {
    var currentMapCellHit = Physics2D.Raycast(transform.position, Vector2.zero, float.PositiveInfinity, Layers.Map);
    if (currentMapCellHit.collider == null)
    {
      throw new Exception("Truck is not above a map cell");
    }

    var currentMapCell = currentMapCellHit.transform.GetComponent<MapCell>();

    (bool success, List<Node> path) = new Dijkstra().Calculate(Map.Graph, Map.Graph[currentMapCell.Coordinates.X, currentMapCell.Coordinates.Y], Map.Graph[cell.Coordinates.X, cell.Coordinates.Y]);

    if (success)
      _lastPath = new Queue<Node>(path);

    //transform.position = cell.Coordinates.ToVector2();
  }

  void OnMouseUp()
  {
    SelectionManager.OnSelection?.Invoke(gameObject);
  }

  void OnDrawGizmos()
  {
    if (_lastPath == null)
      return;

    var last = transform.position;
    foreach (var node in _lastPath)
    {
      var nextPos = new HexCoordinates(node.X, node.Y).ToVector2();

      Gizmos.DrawLine(last, nextPos);

      last = nextPos;
    }
  }
}
