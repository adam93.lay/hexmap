﻿using System;
using UnityEngine;

public class SelectionManager : MonoBehaviour, IClearsState
{
  public GameObject Cursor;
  public static Action<GameObject> OnSelection;

  void Start()
  {
    OnSelection += go =>
    {
      Cursor.gameObject.SetActive(true);
      Cursor.transform.position = go.transform.position + (Vector3.back);
    };
  }

  public bool ClearState()
  {
    bool stateModified = false;

    if (Cursor.gameObject.activeSelf)
    {
      stateModified = true;
      Cursor.gameObject.SetActive(false);
    }

    print("Cleared selection manager state: " + stateModified);

    return stateModified;
  }
}
