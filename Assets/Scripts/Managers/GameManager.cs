﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
  public static GameManager Instance { get; private set; }

  void Awake()
  {
    if (Instance == null)
      Instance = this;
    else
      Destroy(this);

    DateManager.OnNewDay += date =>
    {
      // Snapshot game data?
      // print("It's a new day! " + date);
    };

    MoneyManager.Instance.Balance = 1_000_000F;
  }

  #region Game Time

  public void Pause()
  {
    Time.timeScale = 0;
  }

  public void Play()
  {
    Time.timeScale = 1;
  }

  public void FastForward()
  {
    Time.timeScale = 2;
  }

  #endregion
}
