﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BuildingManager : MonoBehaviour, IClearsState
{
  public static BuildingManager Instance { get; private set; }

  public static Dictionary<string, GameObject> AllBuildingPrefabs = new Dictionary<string, GameObject>();

  void Awake()
  {
    // Singleton
    if (Instance != null)
      Destroy(this);
    else
      Instance = this;

    // Load all buildings
    AllBuildingPrefabs = Resources
      .LoadAll<GameObject>("Prefabs/Buildings")
      .Where(go => go.GetComponent<Building>() != null)
      .ToDictionary(p => p.GetComponent<Building>().Name, p => p);

    OnBuildingComplete += args =>
    {
      // Refresh roads
      args.Building.GetComponent<Waypoint>()?.Refresh(true);
    };
  }

  public static Action<Building> OnBuildingStarted;
  public static Action<BuildingCompleteArgs> OnBuildingComplete;
  public static Action OnBuildingCancelled;

  public Building SelectedBuilding { get; private set; }
  public bool IsBuildingSelected => SelectedBuilding != null;

  public void StartBuilding(Building selected)
  {
    // Only start building if we have enough money
    if (MoneyManager.Instance.Balance < selected.Cost)
      return;

    SelectedBuilding = selected;

    OnBuildingStarted?.Invoke(selected);
  }

  public void FinishBuilding(Building building)
  {
    OnBuildingComplete?.Invoke(new BuildingCompleteArgs
    {
      Building = building,
      ContinueBuilding = building.BuildMultiple,
      Notify = building.NotifyOnComplete
    });

    if (!building.BuildMultiple)
      SelectedBuilding = null;
  }

  public void CancelBuilding()
  {
    SelectedBuilding = null;

    OnBuildingCancelled?.Invoke();
  }

  public bool TryBuild(MapCell mapCell)
  {
    // Are we trying to build something?
    if (SelectedBuilding == null)
    {
      print("BuildingManager.TryBuild: BuildingManager._selectedBuilding is null");
      return false;
    }

    // Can't build if it's not a valid location
    if (!IsValidBuildLocation(mapCell).Valid)
    {
      print("BuildingManager.TryBuild: Location is not valid");
      return false;
    }

    // Only start building if we have enough money
    if (MoneyManager.Instance.Balance < SelectedBuilding.Cost)
    {
      print("BuildingManager.TryBuild: Not enough money");
      return false;
    }

    // Create a new Building from prefab inside the map cell
    GameObject newBuildingGo = Instantiate(SelectedBuilding.gameObject, mapCell.transform, false);

    Building newBuilding = newBuildingGo.GetComponent<Building>();

    // Take the money!
    MoneyManager.Instance.Balance -= newBuilding.Cost;

    FinishBuilding(newBuilding);

    return true;
  }

  public ValidationResult IsValidBuildLocation(MapCell mapCell)
  {
    // is there a map cell?
    if (mapCell == null)
    {
      print("BuildingManager.IsValidBuildLocation: mapCell is null");
      return new ValidationResult(false, "No Map Cell");
    }

    // Does map cell already have building?
    if (mapCell.transform.GetComponentInChildren<Building>() != null)
    {
      print("BuildingManager.IsValidBuildLocation: mapCell already has a building");
      return new ValidationResult(false, "Must be built on unoccupied map cell");
    }

    return new ValidationResult(true);
  }

  public bool ClearState()
  {
    bool stateModified = false;

    if (SelectedBuilding != null)
    {
      stateModified = true;
      CancelBuilding();
    }

    print("Cleared building manager state: " + stateModified);

    return stateModified;
  }
}

public struct BuildingCompleteArgs
{
  public Building Building;
  public bool ContinueBuilding;
  public bool Notify;
}