﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DateManager : MonoBehaviour
{
  public static DateManager Instance { get; private set; }

  private readonly DateTime _origin = new DateTime(2020, 01, 01);
  private readonly float _modifier = 10.0f;
  
  public DateTime Date => _origin + TimeSpan.FromHours(_gameTime);

  private float _gameTime;
  private DateTime _lastDay;

  public static Action<DateTime> OnNewDay;

  void Awake()
  {
    if (Instance == null)
      Instance = this;
    else
      Destroy(this);
  }

  void Update()
  {
    // Increase the in-game time
    _gameTime += Time.deltaTime * _modifier;

    // Get current Date
    DateTime date = Date.Date;

    // If it's a new day...
    if (date > _lastDay)
    {
      // ...then let everyone know
      OnNewDay?.Invoke(date);

      // and set the last date
      _lastDay = date;
    }
  }
}
