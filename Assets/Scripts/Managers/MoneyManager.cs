﻿using System;
using UnityEngine;

public class MoneyManager : MonoBehaviour
{
  public static MoneyManager Instance { get; private set; }

  public static Action<float> OnBalanceChanged;

  private float _balance;
  public float Balance
  {
    get => _balance;
    set
    {
      _balance = value;
      OnBalanceChanged?.Invoke(_balance);
    }
  }

  void Awake()
  {
    if (Instance == null)
      Instance = this;
    else
      Destroy(this);
  }
}
