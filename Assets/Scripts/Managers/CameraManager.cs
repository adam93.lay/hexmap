﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
  public float SpeedX = -0.75f;
  public float SpeedY = -0.75f;
  private Vector3 _lastDrag;

  void Update()
  {
    if (Input.GetMouseButtonDown(2))
    {
      _lastDrag = Input.mousePosition;
    }

    if (Input.GetMouseButton(2))
    {
      Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - _lastDrag);
      Vector3 move = new Vector3(pos.x * SpeedX, pos.y * SpeedY, 0);
      transform.position += move;
    }
  }
}
