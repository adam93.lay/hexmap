﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class InterfaceManager : MonoBehaviour, IClearsState
{
  public Transform MainUI;

  private Dictionary<string, GameObject> InterfacePrefabs;

  private readonly Dictionary<DialogWindow, MapCell> OpenWindows = new Dictionary<DialogWindow, MapCell>();

  private RectTransform _bottomBar;
  private RectTransform _notificationPanel;
  private Tooltip _tooltip;

  void Start()
  {
    // Load UI Prefabs
    InterfacePrefabs = Resources.LoadAll<GameObject>("Prefabs/UI").ToDictionary(p => p.name, p => p);

    _gameManager = FindObjectOfType<GameManager>();

    _notificationPanel = (RectTransform)MainUI.Find("Notifications");
    _tooltip = MainUI.Find("Tooltip").GetComponent<Tooltip>();

    #region Bottom Bar

    _bottomBar = (RectTransform)MainUI.Find("BottomBar");

    #region Play Controls

    var playControls = _bottomBar.Find("PlayControls");
    var btnPause = playControls.Find("Pause").GetComponent<Button>();
    var btnPlay = playControls.Find("Play").GetComponent<Button>();
    var btnFast = playControls.Find("Fast").GetComponent<Button>();

    btnPause.onClick.AddListener(() => { _gameManager.Pause(); });
    btnPlay.onClick.AddListener(() => { _gameManager.Play(); });
    btnFast.onClick.AddListener(() => { _gameManager.FastForward(); });

    #endregion

    #region Buildings Menu

    var buildingsMenu = FindObjectOfType<BuildingsMenu>();

    Button btnBuildings = _bottomBar
      .Find("BuildingsButton")
      .GetComponent<Button>();
    Button btnInfrastructure = _bottomBar
      .Find("InfrastructureButton")
      .GetComponent<Button>();

    btnBuildings.onClick.AddListener(() => { buildingsMenu.Open("Buildings"); });
    btnInfrastructure.onClick.AddListener(() => { buildingsMenu.Open("Infrastructure"); });

    #endregion

    #endregion

    #region Building Manager

    BuildingManager.OnBuildingStarted += building =>
    {
      print($"Selecting {building.Name} to build");
      // Display the tooltip
      _tooltip.SetText(building.Name);
      _tooltip.gameObject.SetActive(true);
    };

    BuildingManager.OnBuildingComplete += (args) =>
    {
      if (!args.ContinueBuilding)
      {
        _tooltip.gameObject.SetActive(false);
      }

      if (args.Notify)
      {
        GameObject notification = Instantiate(InterfacePrefabs["Notification"], _notificationPanel);
        var titleText = notification.transform.Find("Title").Find("Text").GetComponent<Text>();
        var bodyText = notification.transform.Find("Body").GetComponent<Text>();
        
        titleText.text = "Building complete!";
        bodyText.text = args.Building.Name + " has finished building.";
      }
    };

    BuildingManager.OnBuildingCancelled += () =>
    {
      _tooltip.gameObject.SetActive(false);
    };

    #endregion

    #region Selection Manager

    SelectionManager.OnSelection += go =>
    {
      MapCell cell = go.GetComponent<MapCell>();

      // Only interested in map cells, for now...
      if (cell == null)
        return;

      Building building = cell.GetBuilding();

      // Only show windows for buildings, for now...
      // If there's already a window open for this cell
      if (building == null || OpenWindows.Values.Contains(cell))
        return;

      DialogWindow window = DialogWindow.Create(InterfacePrefabs["Window"], MainUI.Find("Windows"));

      GameObject buildingDetailsGo = Instantiate(InterfacePrefabs["BuildingDetails"], window.transform.Find("Body"));
      buildingDetailsGo.GetComponent<BuildingDetailsWindowBody>().Building = building;

      OpenWindows[window] = cell;
    };

    #endregion

    #region Dialog Windows

    DialogWindow.OnWindowClosed += window =>
    {
      OpenWindows.Remove(window);
    };

    #endregion

  }

  private GameManager _gameManager;

  void Update()
  {
    #region Left Mouse Actions

    // Left mouse click
    if (Input.GetMouseButtonDown(0))
    {
      // Get mouse hit
      var mouseHit = InputManager.GetMouseHit(Layers.Map);

      #region Building

      // Are we trying to build?
      // Did the mouse click hit something?
      if (BuildingManager.Instance.IsBuildingSelected && mouseHit.collider != null)
      {
        BuildingManager.Instance.TryBuild(mouseHit.collider.GetComponent<MapCell>());
      }

      #endregion
    }

    #endregion

    #region Right Mouse Actions

    if (Input.GetMouseButtonDown(1))
    {
      BuildingManager.Instance.CancelBuilding();
    }

    #endregion
  }

  public void ShowPauseMenu()
  {
    print("Showing pause menu");
  }

  public bool ClearState()
  {
    bool stateModified = false;

    if (OpenWindows.Count > 0)
    {
      stateModified = true;
      foreach (DialogWindow window in OpenWindows.Keys)
        window.Close();
    }

    // Not worried about state of the tooltip so just hide it and don't set stateModified
    _tooltip.gameObject.SetActive(false);

    print("Cleared interface manager state: " + stateModified);

    return stateModified;
  }
}
