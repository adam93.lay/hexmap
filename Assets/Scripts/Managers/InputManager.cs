﻿using System.Linq;
using UnityEngine;

public class InputManager : MonoBehaviour
{
  private InterfaceManager _interfaceManager;
  private BuildingManager _buildingManager;
  private IClearsState[] _clearable;

  void Start()
  {
    _interfaceManager = FindObjectOfType<InterfaceManager>();
    _clearable = transform
      .parent // Managers
      .GetComponentsInChildren<IClearsState>();
  }

  void Update()
  {
    // Clear state from managers on "esc"
    if (Input.GetKeyDown(KeyCode.Escape))
    {
      bool clearedAnything = _clearable
        .Select(c => c.ClearState())
        .ToList() // Force completing all ClearState() calls first
        .Any(modified => modified);

      if (!clearedAnything)
      {
        _interfaceManager.ShowPauseMenu();
      }
    }
  }

  public static Vector2 GetMousePosition()
  {
    Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    return new Vector2(mousePos.x, mousePos.y);
  }

  public static RaycastHit2D GetMouseHit(int? layerMask = null)
  {
    return layerMask.HasValue
    ? Physics2D.Raycast(GetMousePosition(), Vector2.zero, float.PositiveInfinity, layerMask.Value)
    : Physics2D.Raycast(GetMousePosition(), Vector2.zero);
  }
}
