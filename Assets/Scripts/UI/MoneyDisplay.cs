﻿using UnityEngine;
using UnityEngine.UI;

public class MoneyDisplay : MonoBehaviour
{
  private Text _text;
  private Text Text => _text ?? (_text = GetComponent<Text>());

  void Start()
  {
    MoneyManager.OnBalanceChanged += UpdateBalance;

    UpdateBalance(MoneyManager.Instance.Balance);
  }

  void UpdateBalance(float balance)
  {
    Text.text = balance.ToMoney();
  }
}
