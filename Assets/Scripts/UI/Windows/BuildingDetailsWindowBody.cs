﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BuildingDetailsWindowBody : MonoBehaviour
{
  public Building Building;

  void Start()
  {
    transform.Find("BuildingIcon").GetComponent<Image>().sprite = Building.Sprite;
    transform.Find("BuildingName").GetComponent<TextMeshProUGUI>().text = Building.CustomName;
  }
}
