﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(SlidingPanel))]
public class BuildingsMenu : MonoBehaviour, IClearsState
{
  public GameObject BuildingButtonPrefab;

  private SlidingPanel _slidingPanel;
  private SlidingPanel SlidingPanel => _slidingPanel ?? (_slidingPanel = GetComponent<SlidingPanel>());

  void Start()
  {
    var menuBuildings = (RectTransform)transform.Find("Buildings");
    var menuInfrastructure = (RectTransform)transform.Find("Infrastructure");

    void AddButton(Building b, Transform menu)
    {
      GameObject buttonGo = Instantiate(BuildingButtonPrefab, menu);

      buttonGo.transform.Find("BuildingIcon").GetComponent<Image>().sprite = b.Sprite;

      buttonGo.GetComponent<Button>().onClick.AddListener(() =>
      {
        // Set the selected building
        BuildingManager.Instance.StartBuilding(b);
      });
    }

    void AddButtonsFromPath(string path, Transform menu)
    {
      foreach (Building b in Resources.LoadAll<Building>(path))
      {
        AddButton(b, menu);
      }
    }

    AddButtonsFromPath("Prefabs/Buildings/Buildings", menuBuildings);
    AddButtonsFromPath("Prefabs/Buildings/Infrastructure", menuInfrastructure);

    BuildingManager.OnBuildingCancelled += Close;
  }

  void Update()
  {
    if (Input.GetKeyDown(KeyCode.F1))
    {
      Open("Buildings");
    }

    if (Input.GetKeyDown(KeyCode.F2))
    {
      Open("Infrastructure");
    }
  }

  private bool IsOpen => name.EndsWith("_Open");

  public void Open(string menuName)
  {
    foreach (Transform child in transform)
    {
      child.gameObject.SetActive(child.name == menuName);
    }

    if (IsOpen)
      return;

    name += "_Open";

    // Slide to above bottom menu
    SlidingPanel.BeginSlideTo(new Vector2(0, 65f));
  }

  public void Close()
  {
    // Return if it's not open
    if (!IsOpen)
      return;

    name = name.Replace("_Open", "");

    // Slide off screen
    SlidingPanel.BeginSlideTo(new Vector2(0, -65f));
  }

  public bool ClearState()
  {
    bool stateModified = false;

    if (IsOpen)
    {
      stateModified = true;
      Close();
    }

    print("Cleared building menu state: " + stateModified);

    return stateModified;
  }
}
