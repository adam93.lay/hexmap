﻿
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BuildingPreview : MonoBehaviour
{
  private readonly Color _colourValid = ColourHelper.FromRgba(0x80, 0xFF, 0x80);
  private readonly Color _colourInvalid = ColourHelper.FromRgba(0xFF, 0x80, 0x80);
  private readonly Color _colourHidden = ColourHelper.FromRgba(0xFF, 0xFF, 0xFF, 0f);
  private List<SpriteRenderer> _radiusHighlighted;

  private SpriteRenderer _spriteRenderer;
  private SpriteRenderer SpriteRenderer => _spriteRenderer ?? (_spriteRenderer = GetComponent<SpriteRenderer>());

  private BuildingManager _buildingManager;
  private BuildingManager BuildingManager => _buildingManager ?? (_buildingManager = FindObjectOfType<BuildingManager>());

  private Tooltip _tooltip;
  private Tooltip Tooltip => _tooltip ?? (_tooltip = FindObjectOfType<Tooltip>());

  private Building _currentBuilding;

  void Awake()
  {
    BuildingManager.OnBuildingStarted += building =>
    {
      _currentBuilding = building;
      SpriteRenderer.sprite = building.Sprite;
      gameObject.SetActive(true);
    };

    void OnBuildingStopped()
    {
      _currentBuilding = null;
      ClearRadiusHighlights();
      gameObject.SetActive(false);
    }

    BuildingManager.OnBuildingCancelled += OnBuildingStopped;
    BuildingManager.OnBuildingComplete += args =>
    {
      if (!args.ContinueBuilding)
        OnBuildingStopped();
    };

    // Start disabled
    gameObject.SetActive(false);
  }

  private void ClearRadiusHighlights()
  {
    // Clear any radius highlights
    if (_radiusHighlighted?.Any() ?? false)
      foreach (var sr in _radiusHighlighted)
        sr.color = Color.white;
  }

  void Update()
  {
    ClearRadiusHighlights();

    // If the building preview is active, then snap it to the cell the cursor is over
    if (!gameObject.activeSelf)
      return;

    var mouseHit = InputManager.GetMouseHit(Layers.Map);
    if (mouseHit.collider != null)
    {
      var mapCell = mouseHit.collider.GetComponent<MapCell>();

      // Are we over a map cell?
      if (mapCell != null)
      {
        // Move to the map cell (in front of it)
        transform.position = mouseHit.collider.transform.position + Vector3.back;

        ValidationResult validationResult = BuildingManager.IsValidBuildLocation(mapCell);

        // Is it valid?
        SpriteRenderer.color = validationResult.Valid
          ? _colourValid
          : _colourInvalid;

        // Update tooltip text
        Tooltip.SetText(validationResult.Valid
          ? _currentBuilding.Cost.ToMoney()
          : validationResult.Error);

        int range = _currentBuilding.MapRange;

        if (range > 0)
        {
          _radiusHighlighted = new List<SpriteRenderer>();

          float radius = HexHelper.InnerRadius * (range * 2 - 1);
          foreach (Collider2D hit in Physics2D.OverlapCircleAll(mapCell.transform.position, radius, Layers.Map))
          {
            var sr = hit.GetComponent<SpriteRenderer>();
            if (sr == null)
              continue;

            _radiusHighlighted.Add(sr);
            sr.color = ColourHelper.FromRgba(192, 192, 255);
          }
        }
      }
      else
      {
        SpriteRenderer.color = _colourHidden;
      }
    }
    else
    {
      SpriteRenderer.color = _colourHidden;
    }
  }
}
