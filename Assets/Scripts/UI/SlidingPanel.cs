﻿using System;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class SlidingPanel : MonoBehaviour
{
  private Coroutine _slideCoroutine;

  public void BeginSlideTo(Vector2 slideTo, Action onStart = null, Action onComplete = null)
  {
    if (_slideCoroutine != null)
      StopCoroutine(_slideCoroutine);

    _slideCoroutine = StartCoroutine(SlideTo(slideTo, onStart, onComplete));
  }


  private IEnumerator SlideTo(Vector2 to, Action onStart = null, Action onComplete = null)
  {
    onStart?.Invoke();

    Vector2 lerpStart = ((RectTransform)transform).anchoredPosition;
    float lerpTime = 0;
    while (lerpTime < 1)
    {
      lerpTime = Mathf.Clamp01(lerpTime + Time.deltaTime * 4);

      ((RectTransform)transform).anchoredPosition = Vector2.Lerp(lerpStart, to, lerpTime / 1f);

      yield return null;
    }

    onComplete?.Invoke();
  }
}
