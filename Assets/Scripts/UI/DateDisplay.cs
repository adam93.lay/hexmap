﻿using UnityEngine;
using UnityEngine.UI;

public class DateDisplay : MonoBehaviour
{
  public string Format = "yyyy-MM-dd";

  private Text _dateText;
  private Text DateText => _dateText ?? (_dateText = GetComponent<Text>());

  void Update()
  {
    DateText.text = DateManager.Instance.Date.ToString(Format);
  }
}
