﻿using TMPro;
using UnityEngine;

public class Tooltip : MonoBehaviour
{
  private TextMeshProUGUI _text;
  private TextMeshProUGUI Text => _text ?? (_text = transform.GetComponentInChildren<TextMeshProUGUI>());

  public void SetText(string text)
  {
    Text.text = text;
  }

  void Update()
  {
    if (!gameObject.activeSelf)
      return;

    Vector3 offset = Vector3.up * 10;

    //((RectTransform)transform).positi
    transform.position = Input.mousePosition + offset;
  }
}
