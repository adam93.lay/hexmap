﻿using System;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class FadingPanel : MonoBehaviour {

  void Awake()
  {
    _canvasGroup = GetComponent<CanvasGroup>();
  }

  private CanvasGroup _canvasGroup;
  private Coroutine _fadeCoroutine;

  public void FadeIn(Action onComplete = null)
  {
    if (_fadeCoroutine != null)
      StopCoroutine(_fadeCoroutine);

    _fadeCoroutine = StartCoroutine(FadeFromTo(0.0f, 1.0f, onComplete));
  }

  public void FadeOut(Action onComplete = null)
  {
    if (_fadeCoroutine != null)
      StopCoroutine(_fadeCoroutine);

    _fadeCoroutine = StartCoroutine(FadeFromTo(1.0f, 0.0f, onComplete));
  }

  private IEnumerator FadeFromTo(float from, float to, Action onComplete = null)
  {
    float lerpTime = 0;
    while (lerpTime < 1)
    {
      lerpTime = Mathf.Clamp01(lerpTime + Time.deltaTime * 4);
      _canvasGroup.alpha = Mathf.Lerp(from, to, lerpTime / 1f);
      yield return null;
    }

    onComplete?.Invoke();
  }
}
