﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(FadingPanel))]
public class Notification : MonoBehaviour
{
  private FadingPanel _fadingPanel;

  void Awake()
  {
    _fadingPanel = GetComponent<FadingPanel>();

    GetComponentInChildren<Button>()
      .onClick
      .AddListener(Close);
  }

  private void Close()
  {
    _closing = true;

    _fadingPanel.FadeOut(() =>
    {
      // Delete self
      Destroy(gameObject);
    });
  }

  void Start()
  {
    _fadingPanel.FadeIn();
  }

  private float _lifetime;
  private float _closeAfter = 15f;
  private bool _closing = false;

  void Update()
  {
    _lifetime += Time.deltaTime;

    if (!_closing && _lifetime > _closeAfter)
      Close();
  }
}
