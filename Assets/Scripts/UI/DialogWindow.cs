﻿using System;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(FadingPanel))]
public class DialogWindow : MonoBehaviour
{
  private FadingPanel _fadingPanel;

  public static Action<DialogWindow> OnWindowClosed;

  void Awake()
  {
    _fadingPanel = GetComponent<FadingPanel>();

    GetComponentInChildren<Button>()
      .onClick
      .AddListener(Close);
  }

  public void Close()
  {
    _fadingPanel.FadeOut(() =>
    {
      OnWindowClosed?.Invoke(this);

      // Delete self
      Destroy(gameObject);
    });
  }

  public static DialogWindow Create(GameObject prefab, Transform parent)
  {
    GameObject windowGo = Instantiate(prefab, parent);
    return windowGo.GetComponent<DialogWindow>();
  }
}
